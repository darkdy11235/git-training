# Change Log

All notable changes to this project will be documented in this file. 

## [Unreleased]

- Feature - Addition
  - Implement addition.

- Feature - Subtraction
  - Implement subtraction.

- Feature - Multiplication
  - Implement multiplication.


## [v1.0.0] - 2024-04-12

- Initial Release
  - Initial release from develop.

- Bug Fix:
  - Implement division.

- Feature - Mod
  - Implement mod.

- Hot Fix:
  - Create hotfix/v1.0.1

- Bug Fix:
  - Remove addition feature